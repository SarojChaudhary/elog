﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Elog.Domain.Entities.BaseEntity
{
    public class BaseEntity
    {
        [Key]
        public int CompanyId { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedTs { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedTs { get; set; }
    }
}
